<?php
include('../day12/conection.php');
session_start();
$genderArray = array(0 => "Nam", 1 => "Nữ");
$departmentArray = array("EMPTY" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
$studentInfo = array();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="confirm.css">
</head>

<body>

    <div class='login-content'>
        <?php 
            if(isset($_POST['submit'])){
                $id = $_SESSION["id"];
                echo $id;
                $time = strtotime($_SESSION["birthday"]);
                $date = date('Y-m-d',$time);
                array_push($studentInfo,$id
                ,$_SESSION["studentName"]
                ,(int)$_SESSION["gender"]
                ,$_SESSION["department"]
                ,$date
                ,$_SESSION["address"]
                ,$_SESSION["image-upload"]);
                updateStudent($studentInfo);
                header('location: complete_regist.php');
            }
        ?>
        
        <form class='form-input' method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Họ và tên </label>
                </div>
                <p class='input-item'>
                    <?php
                    echo  $_SESSION["studentName"];
                    ?>
                </p>
            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Giới tính</label>
                </div>
                <p class='input-item'>
                <?php
                    echo  $genderArray[$_SESSION["gender"]];
                    ?>
                </p>
            </div>
            <div class="content">
                <div class="lb">
                    <label class="label-star">Phân khoa</label>
                </div>
                <p class='input-item'>
                    <?php
                    echo $departmentArray[$_SESSION["department"]];
                    ?></p>

            </div>
            <div class="content">
                <div class='lb'>
                    <label class="label-star">Ngày sinh</label>
                </div>
                <p class='input-item'>
                    <?php
                    echo $_SESSION["birthday"];
                    ?></p>

            </div>
            <div class="content">
                <div class='lb'>
                    <label>Địa chỉ </label>
                </div>
                <p class='input-item'>
                    <?php
                    echo $_SESSION["address"];
                    ?></p>
            </div>

            <div class="content">
                <div class='lb'>
                    <label>Hình ảnh</label>
                </div>
                <div id="image">
                    <?php
                    if(isset($_SESSION["image-upload"])){
                        $src_image = $_SESSION["image-upload"];
                        echo "<img src= \"$src_image\" width=\"100\" height=\"50\" >";
                    }                
                    
                    ?>
                </div>
                
             
            </div>

            <input type='submit' value='Xác nhận' id='btn-submit' name="submit">
        </form>


    </div>


</body>

</html>