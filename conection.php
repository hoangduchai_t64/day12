<?php

$checkConnected = false;
$server = "localhost";
  $userName = "root";
  $password = "";
  $database = "mysql";
  $con = mysqli_connect($server,$userName,$password);

function connectDatabase(){
  global $checkConnected , $con;
  // Check connection

  if ($con -> connect_error) {
    echo "Failed to connect to MySQL: " . $con -> connect_error;
    exit();
  }else{
    createDatabase($con);
    $checkConnected = true;
  }
}
function createDatabase($con){
  $sql = "CREATE DATABASE IF NOT EXISTS day12";
  if(mysqli_query($con,$sql)){
    // echo "Database created successfully";
    createTable($con);
  } else {
  // ec ho "Error creating database: " . mysqli_error($con);
  }
}
function createTable($con)
{

  $sql = "CREATE TABLE IF NOT EXISTS day12.student (
    id int(11) NOT NULL,
    name varchar(250) NOT NULL,
    gender int(1) NOT NULL,
    faculty char(3) NOT NULL,
    birthday datetime NOT NULL,
    address varchar(250) DEFAULT NULL,
    avartar text DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

  if ($con->query($sql) === TRUE ) {
    // echo "Table MyGuests created successfully";
  } else {
    // echo "Error creating table: " . $con->error;
  }

}
function updateStudent($studentInfo){
  global $checkConnected, $con;
  if($checkConnected == false){
    connectDatabase();
  }
  $studentInfo[1] = "'". $studentInfo[1] ."'";
  $studentInfo[3] = "'". $studentInfo[3] ."'";
  $studentInfo[4] = "'". $studentInfo[4] ."'";
  $studentInfo[5] = "'". $studentInfo[5] ."'";
  $studentInfo[6] = "'". $studentInfo[6] ."'";
  $sql = "INSERT INTO day12.student (id, name, gender, faculty,birthday,address,avartar)
  VALUES ($studentInfo[0], $studentInfo[1], $studentInfo[2], $studentInfo[3],$studentInfo[4],$studentInfo[5],$studentInfo[6])";
  if (count($studentInfo) > 0 && $con->query($sql) === TRUE) {
    // echo "Insert student successfully";
  } else {
    // echo "Error insert student: " . $con->error;
  }
  $con -> close();
}
function getListStudent(){
  global $checkConnected, $con;
  if($checkConnected == false){
    connectDatabase();
  }
  $listStudent = array();
  $sql = "SELECT * FROM day12.student";
  $result = mysqli_query($con,$sql);
  for ($x = 0; $x < mysqli_num_rows($result); $x++){
    $row = mysqli_fetch_assoc($result);  
    $student = array($row['id'],$row['name'],$row['gender'],$row['faculty'],$row['birthday'],$row['address'],$row['avartar']);
    $listStudent[] = $student;
  }
  return $listStudent;
}
function getId(){
  $listStudent = getListStudent();
  echo $listStudent[count($listStudent)-1][0];
  return $listStudent[count($listStudent)-1][0];
}
?>
